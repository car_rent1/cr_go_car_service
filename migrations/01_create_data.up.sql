CREATE TABLE "brand" (
  "id" UUID PRIMARY KEY NOT NULL,
  "photo" varchar,
  "name" varchar(45),
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "model" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar(45),
  "brand_id" UUID,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "car"(
    "id" UUID PRIMARY KEY,
    "brand_id" UUID ,
    "model_id" UUID ,
    "investor_id" UUID ,
    "state_number" VARCHAR NOT NULL,
    "mileage" NUMERIC NOT NULL,
    "status" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE "car_report"(
  "state_number" VARCHAR,
  "car_id" UUID REFERENCES "car"("id"),
  "status" VARCHAR,
  "date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);