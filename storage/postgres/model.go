package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/helper"
)

type modelRepo struct {
	db *pgxpool.Pool
}

func NewModelRepo(db *pgxpool.Pool) *modelRepo {
	return &modelRepo{
		db: db,
	}
}

func (c *modelRepo) Create(ctx context.Context, req *car_service.CreateModel) (resp *car_service.ModelPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "model" (
				id,
				name,
				brand_id
			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.BrandId,
	)
	if err != nil {
		return nil, err
	}

	return &car_service.ModelPrimaryKey{Id: id.String()}, nil

}

func (c *modelRepo) GetByID(ctx context.Context, req *car_service.ModelPrimaryKey) (resp *car_service.Model, err error) {
	query := `
		SELECT
			id,
			name,
			brand_id,
			created_at,
			updated_at
		FROM "model"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		brand_id  sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&brand_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &car_service.Model{
		Id:        id.String,
		Name:      name.String,
		BrandId:   brand_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *modelRepo) GetList(ctx context.Context, req *car_service.GetListModelRequest) (resp *car_service.GetListModelResponse, err error) {

	resp = &car_service.GetListModelResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			brand_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "model"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			brand_id  sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&brand_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Models = append(resp.Models, &car_service.Model{
			Id:        id.String,
			Name:      name.String,
			BrandId:   brand_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()

	return
}

func (c *modelRepo) Update(ctx context.Context, req *car_service.UpdateModel) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "model"
			SET
				name = :name,
				brand_id = :brand_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":       req.GetId(),
		"name":     req.GetName(),
		"brand_id": req.GetBrandId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *modelRepo) Delete(ctx context.Context, req *car_service.ModelPrimaryKey) (*car_service.ModelEmpty, error) {

	query := `DELETE FROM "model" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &car_service.ModelEmpty{}, nil
}
