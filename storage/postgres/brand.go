package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/helper"
)

type brandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *brandRepo {
	return &brandRepo{
		db: db,
	}
}

func (c *brandRepo) Create(ctx context.Context, req *car_service.CreateBrand) (resp *car_service.BrandPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "brand" (
				id,
				name,
				photo
			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Photo,
	)
	if err != nil {
		return nil, err
	}

	return &car_service.BrandPrimaryKey{Id: id.String()}, nil

}

func (c *brandRepo) GetByID(ctx context.Context, req *car_service.BrandPrimaryKey) (resp *car_service.Brand, err error) {
	query := `
		SELECT
			id,
			name,
			photo,
			created_at,
			updated_at
		FROM "brand"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		photo     sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &car_service.Brand{
		Id:        id.String,
		Name:      name.String,
		Photo:     photo.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *brandRepo) GetList(ctx context.Context, req *car_service.GetListBrandRequest) (resp *car_service.GetListBrandResponse, err error) {

	resp = &car_service.GetListBrandResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			photo,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "brand"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			photo     sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&photo,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Brands = append(resp.Brands, &car_service.Brand{
			Id:        id.String,
			Name:      name.String,
			Photo:     photo.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *brandRepo) Update(ctx context.Context, req *car_service.UpdateBrand) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "brand"
			SET
				name = :name,
				photo = :photo,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":    req.GetId(),
		"name":  req.GetName(),
		"photo": req.GetPhoto(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *brandRepo) Delete(ctx context.Context, req *car_service.BrandPrimaryKey) (*car_service.BrandEmpty, error) {

	query := `DELETE FROM "brand" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &car_service.BrandEmpty{}, nil
}
