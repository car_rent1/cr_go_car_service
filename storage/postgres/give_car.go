package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type GiveCarRepo struct {
	db *pgxpool.Pool
}

func NewGiveCarRepo(db *pgxpool.Pool) storage.GiveCarRepoI {
	return &GiveCarRepo{
		db: db,
	}
}

func (c *GiveCarRepo) Create(ctx context.Context, req *car_service.CreateGiveCar) (resp *car_service.GiveCarPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "give_car" (
				id,
				order_id,
				mechanik_id,
				mileage,
				gas,
				photo
			) VALUES ($1, $2, $3, $4, $5, $6)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.MechanikId,
		req.Mileage,
		req.Gas,
		req.Photo,
	)
	if err != nil {
		return nil, err
	}

	return &car_service.GiveCarPrimaryKey{Id: id.String()}, nil

}

func (c *GiveCarRepo) GetByPKey(ctx context.Context, req *car_service.GiveCarPrimaryKey) (resp *car_service.GiveCar, err error) {
	query := `
		SELECT
			id,
			order_id,
			mechanik_id,
			mileage,
			gas,
			photo,
			created_at,
			updated_at
		FROM "give_car"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		order_id    sql.NullString
		mechanik_id sql.NullString
		milleage    int
		gas         int
		photo       sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&mechanik_id,
		&milleage,
		&gas,
		&photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &car_service.GiveCar{
		Id:         id.String,
		OrderId:    order_id.String,
		MechanikId: mechanik_id.String,
		Mileage:    int64(milleage),
		Gas:        int64(gas),
		Photo:      photo.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *GiveCarRepo) GetAll(ctx context.Context, req *car_service.GetListGiveCarRequest) (resp *car_service.GetListGiveCarResponse, err error) {

	resp = &car_service.GetListGiveCarResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,
			mechanik_id,
			mileage,
			gas,
			photo,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "give_car"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			order_id    sql.NullString
			mechanik_id sql.NullString
			milleage    int
			gas         int
			photo       sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&mechanik_id,
			&milleage,
			&gas,
			&photo,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.GiveCars = append(resp.GiveCars, &car_service.GiveCar{
			Id:         id.String,
			OrderId:    order_id.String,
			MechanikId: mechanik_id.String,
			Mileage:    int64(milleage),
			Gas:        int64(gas),
			Photo:      photo.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *GiveCarRepo) Update(ctx context.Context, req *car_service.UpdateGiveCar) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "give_car"
			SET
				order_id = :order_id,
				mechanik_id = :mechanik_id,
				mileage = :mileage,
				gas = :gas,
				photo = :photo,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"order_id":    req.GetOrderId(),
		"mechanik_id": req.GetMechanikId(),
		"mileage":     req.GetMileage(),
		"gas":         req.GetGas(),
		"photo":       req.GetPhoto(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *GiveCarRepo) Delete(ctx context.Context, req *car_service.GiveCarPrimaryKey) (*car_service.GiveCarEmpy, error) {

	query := `DELETE FROM "give_car" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &car_service.GiveCarEmpy{}, nil
}
