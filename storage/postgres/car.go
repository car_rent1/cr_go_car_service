package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type CarRepo struct {
	db *pgxpool.Pool
}

func NewCarRepo(db *pgxpool.Pool) storage.CarRepoI {
	return &CarRepo{
		db: db,
	}
}

func (c *CarRepo) Create(ctx context.Context, req *car_service.CreateCar) (resp *car_service.CarPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "car" (
				id,
				brand_id,  
				model_id,  
				state_number,
				mileage,
				investor_id,
				status
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BrandId,
		req.ModelId,
		req.StateNumber,
		req.Milleage,
		req.InvestorId,
		"INSTOCK",
	)
	if err != nil {
		return nil, err
	}

	return &car_service.CarPrimaryKey{Id: id.String()}, nil

}

func (c *CarRepo) GetByPKey(ctx context.Context, req *car_service.CarPrimaryKey) (resp *car_service.Car, err error) {
	query := `
		SELECT
			id,
			brand_id,  
			model_id,  
			state_number,
			mileage,
			investor_id,
			status,
			created_at,
			updated_at
		FROM "car"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		brand_id     sql.NullString
		model_id     sql.NullString
		state_number sql.NullString
		milleage     int
		investor_id  sql.NullString
		status       sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brand_id,
		&model_id,
		&state_number,
		&milleage,
		&investor_id,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &car_service.Car{
		Id:          id.String,
		BrandId:     brand_id.String,
		ModelId:     model_id.String,
		StateNumber: state_number.String,
		Milleage:    int64(milleage),
		InvestorId:  investor_id.String,
		Status:      status.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *CarRepo) GetAll(ctx context.Context, req *car_service.GetListCarRequest) (resp *car_service.GetListCarResponse, err error) {

	resp = &car_service.GetListCarResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,  
			model_id,  
			state_number,
			mileage,
			investor_id ,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "car"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchBrand != "" {
		filter += " AND brand_id = '" + req.SearchBrand + "'"
	}
	if req.SearchInvestor != "" {
		filter += " AND investor_id = '" + req.SearchInvestor + "'"
	}
	if req.SearchModel != "" {
		filter += " AND model_id = '" + req.SearchModel + "'"
	}
	if req.SearchStateNumber != "" {
		filter += " AND state_number = '" + req.SearchStateNumber + "'"
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			brand_id     sql.NullString
			model_id     sql.NullString
			state_number sql.NullString
			milleage     int
			investor_id  sql.NullString
			status       sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brand_id,
			&model_id,
			&state_number,
			&milleage,
			&investor_id,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Cars = append(resp.Cars, &car_service.Car{
			Id:          id.String,
			BrandId:     brand_id.String,
			ModelId:     model_id.String,
			StateNumber: state_number.String,
			Milleage:    int64(milleage),
			InvestorId:  investor_id.String,
			Status:      status.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *CarRepo) Update(ctx context.Context, req *car_service.UpdateCar) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "car"
			SET
				brand_id = :brand_id,
				model_id = :model_id,
				state_number = :state_number,
				mileage = :mileage,
				investor_id = :investor_id,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"brand_id":     req.GetBrandId(),
		"model_id":     req.GetModelId(),
		"state_number": req.GetStateNumber(),
		"mileage":      req.GetMilleage(),
		"investor_id":  req.GetInvestorId(),
		"status":       req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CarRepo) Delete(ctx context.Context, req *car_service.CarPrimaryKey) (*car_service.CarEmpty, error) {

	query := `DELETE FROM "car" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &car_service.CarEmpty{}, nil
}
