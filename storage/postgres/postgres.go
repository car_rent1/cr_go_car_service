package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type Store struct {
	db          *pgxpool.Pool
	brand       *brandRepo
	model       *modelRepo
	car         storage.CarRepoI
	carActivity storage.CarActivityRepoI
	giveCar     storage.GiveCarRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Brand() storage.BrandRepoI {

	if s.brand == nil {
		s.brand = NewBrandRepo(s.db)
	}

	return s.brand

}
func (s *Store) Model() storage.ModelRepoI {
	if s.model == nil {
		s.model = NewModelRepo(s.db)
	}

	return s.model
}
func (s *Store) Car() storage.CarRepoI {
	if s.car == nil {
		s.car = NewCarRepo(s.db)
	}
	return s.car
}
func (s *Store) CarActivity() storage.CarActivityRepoI {
	if s.carActivity == nil {
		s.carActivity = NewCarActivityRepo(s.db)
	}
	return s.carActivity
}
func (s *Store) GiveCar() storage.GiveCarRepoI {
	if s.giveCar == nil {
		s.giveCar = NewGiveCarRepo(s.db)
	}
	return s.giveCar
}
