package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type CarActivityRepo struct {
	db *pgxpool.Pool
}

func NewCarActivityRepo(db *pgxpool.Pool) storage.CarActivityRepoI {
	return &CarActivityRepo{
		db: db,
	}
}

func (c *CarActivityRepo) Create(ctx context.Context, req *car_service.CarActivityCreate) (*car_service.CarActivity, error) {
	query := `INSERT INTO "car_report" (
		state_number,  
		car_id,
		status
	) VALUES ($1, $2, $3)
`
	_, err := c.db.Exec(ctx,
		query,
		req.StateNumber,
		req.CarId,
		req.Status,
	)
	fmt.Println("==============================================")

	if err != nil {
		return nil, err
	}
	fmt.Println("==============================================")
	return &car_service.CarActivity{StateNumber: req.StateNumber, CarId: req.CarId, Status: req.Status}, nil
}

func (c *CarActivityRepo) GetAll(ctx context.Context, req *car_service.CarActivityGetListRequest) (resp *car_service.CarActivityResp, err error) {
	resp = &car_service.CarActivityResp{}

	var (
		query   string
		limit   = ""
		offset  = " OFFSET 0 "
		params  = make(map[string]interface{})
		filter  = " WHERE TRUE"
		from    = req.From
		to      = req.To
		groupBy = " GROUP BY state_number, car_id, status, date"
		sort    = " ORDER BY date(date)"
	)

	query = `
		SELECT
			state_number,  
			car_id,  
			status,
			date
		FROM "car_report"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.From != "" && req.To != "" {
		query += filter + from + to + sort + groupBy + offset + limit
	} else {
		query += filter + sort + groupBy + offset + limit
	}

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			state_number sql.NullString
			car_id       sql.NullString
			status       sql.NullString
			date         sql.NullString
		)

		err := rows.Scan(
			&state_number,
			&car_id,
			&status,
			&date,
		)

		if err != nil {
			return resp, err
		}

		resp.Res = append(resp.Res, &car_service.CarActivity{

			StateNumber: state_number.String,
			CarId:       car_id.String,
			Status:      status.String,
			Date:        date.String,
		})
	}
	defer rows.Close()
	return
}
