package storage

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
)

type StorageI interface {
	CloseDB()
	Brand() BrandRepoI
	Model() ModelRepoI
	Car() CarRepoI
	CarActivity() CarActivityRepoI
	GiveCar() GiveCarRepoI
}
type BrandRepoI interface {
	Create(context.Context, *car_service.CreateBrand) (*car_service.BrandPrimaryKey, error)
	GetByID(context.Context, *car_service.BrandPrimaryKey) (*car_service.Brand, error)
	GetList(context.Context, *car_service.GetListBrandRequest) (*car_service.GetListBrandResponse, error)
	Update(context.Context, *car_service.UpdateBrand) (int64, error)
	Delete(context.Context, *car_service.BrandPrimaryKey) (*car_service.BrandEmpty, error)
}

type ModelRepoI interface {
	Create(context.Context, *car_service.CreateModel) (*car_service.ModelPrimaryKey, error)
	GetByID(context.Context, *car_service.ModelPrimaryKey) (*car_service.Model, error)
	GetList(context.Context, *car_service.GetListModelRequest) (*car_service.GetListModelResponse, error)
	Update(context.Context, *car_service.UpdateModel) (int64, error)
	Delete(context.Context, *car_service.ModelPrimaryKey) (*car_service.ModelEmpty, error)
}

type CarRepoI interface {
	Create(ctx context.Context, req *car_service.CreateCar) (resp *car_service.CarPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *car_service.CarPrimaryKey) (resp *car_service.Car, err error)
	GetAll(ctx context.Context, req *car_service.GetListCarRequest) (resp *car_service.GetListCarResponse, err error)
	Update(ctx context.Context, req *car_service.UpdateCar) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *car_service.CarPrimaryKey) (*car_service.CarEmpty, error)
}

type CarActivityRepoI interface {
	Create(cxt context.Context, req *car_service.CarActivityCreate) (resp *car_service.CarActivity, err error)
	GetAll(cxt context.Context, req *car_service.CarActivityGetListRequest) (resp *car_service.CarActivityResp, err error)
}

type GiveCarRepoI interface {
	Create(ctx context.Context, req *car_service.CreateGiveCar) (resp *car_service.GiveCarPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *car_service.GiveCarPrimaryKey) (resp *car_service.GiveCar, err error)
	GetAll(ctx context.Context, req *car_service.GetListGiveCarRequest) (resp *car_service.GetListGiveCarResponse, err error)
	Update(ctx context.Context, req *car_service.UpdateGiveCar) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *car_service.GiveCarPrimaryKey) (*car_service.GiveCarEmpy, error)
}
