// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: tarif.proto

package order_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Tarif struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Photo          string `protobuf:"bytes,2,opt,name=photo,proto3" json:"photo,omitempty"`
	ModelId        string `protobuf:"bytes,3,opt,name=model_id,json=modelId,proto3" json:"model_id,omitempty"`
	Name           string `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Price          int64  `protobuf:"varint,5,opt,name=price,proto3" json:"price,omitempty"`
	DayLimit       int64  `protobuf:"varint,6,opt,name=day_limit,json=dayLimit,proto3" json:"day_limit,omitempty"`
	OverLimit      int64  `protobuf:"varint,7,opt,name=over_limit,json=overLimit,proto3" json:"over_limit,omitempty"`
	InsurancePrice int64  `protobuf:"varint,8,opt,name=insurance_price,json=insurancePrice,proto3" json:"insurance_price,omitempty"`
	Description    string `protobuf:"bytes,9,opt,name=description,proto3" json:"description,omitempty"`
	Status         bool   `protobuf:"varint,10,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt      string `protobuf:"bytes,11,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt      string `protobuf:"bytes,12,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Tarif) Reset() {
	*x = Tarif{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Tarif) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Tarif) ProtoMessage() {}

func (x *Tarif) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Tarif.ProtoReflect.Descriptor instead.
func (*Tarif) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{0}
}

func (x *Tarif) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Tarif) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *Tarif) GetModelId() string {
	if x != nil {
		return x.ModelId
	}
	return ""
}

func (x *Tarif) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Tarif) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *Tarif) GetDayLimit() int64 {
	if x != nil {
		return x.DayLimit
	}
	return 0
}

func (x *Tarif) GetOverLimit() int64 {
	if x != nil {
		return x.OverLimit
	}
	return 0
}

func (x *Tarif) GetInsurancePrice() int64 {
	if x != nil {
		return x.InsurancePrice
	}
	return 0
}

func (x *Tarif) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *Tarif) GetStatus() bool {
	if x != nil {
		return x.Status
	}
	return false
}

func (x *Tarif) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Tarif) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateTarif struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status         bool   `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	Photo          string `protobuf:"bytes,2,opt,name=photo,proto3" json:"photo,omitempty"`
	ModelId        string `protobuf:"bytes,3,opt,name=model_id,json=modelId,proto3" json:"model_id,omitempty"`
	Name           string `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Price          int64  `protobuf:"varint,5,opt,name=price,proto3" json:"price,omitempty"`
	DayLimit       int64  `protobuf:"varint,6,opt,name=day_limit,json=dayLimit,proto3" json:"day_limit,omitempty"`
	OverLimit      int64  `protobuf:"varint,7,opt,name=over_limit,json=overLimit,proto3" json:"over_limit,omitempty"`
	InsurancePrice int64  `protobuf:"varint,8,opt,name=insurance_price,json=insurancePrice,proto3" json:"insurance_price,omitempty"`
	Description    string `protobuf:"bytes,9,opt,name=description,proto3" json:"description,omitempty"`
}

func (x *CreateTarif) Reset() {
	*x = CreateTarif{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateTarif) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateTarif) ProtoMessage() {}

func (x *CreateTarif) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateTarif.ProtoReflect.Descriptor instead.
func (*CreateTarif) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{1}
}

func (x *CreateTarif) GetStatus() bool {
	if x != nil {
		return x.Status
	}
	return false
}

func (x *CreateTarif) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *CreateTarif) GetModelId() string {
	if x != nil {
		return x.ModelId
	}
	return ""
}

func (x *CreateTarif) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateTarif) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *CreateTarif) GetDayLimit() int64 {
	if x != nil {
		return x.DayLimit
	}
	return 0
}

func (x *CreateTarif) GetOverLimit() int64 {
	if x != nil {
		return x.OverLimit
	}
	return 0
}

func (x *CreateTarif) GetInsurancePrice() int64 {
	if x != nil {
		return x.InsurancePrice
	}
	return 0
}

func (x *CreateTarif) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

type UpdateTarif struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Photo          string `protobuf:"bytes,2,opt,name=photo,proto3" json:"photo,omitempty"`
	ModelId        string `protobuf:"bytes,3,opt,name=model_id,json=modelId,proto3" json:"model_id,omitempty"`
	Name           string `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Price          int64  `protobuf:"varint,5,opt,name=price,proto3" json:"price,omitempty"`
	DayLimit       int64  `protobuf:"varint,6,opt,name=day_limit,json=dayLimit,proto3" json:"day_limit,omitempty"`
	OverLimit      int64  `protobuf:"varint,7,opt,name=over_limit,json=overLimit,proto3" json:"over_limit,omitempty"`
	InsurancePrice int64  `protobuf:"varint,8,opt,name=insurance_price,json=insurancePrice,proto3" json:"insurance_price,omitempty"`
	Description    string `protobuf:"bytes,9,opt,name=description,proto3" json:"description,omitempty"`
	Status         bool   `protobuf:"varint,10,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *UpdateTarif) Reset() {
	*x = UpdateTarif{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateTarif) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateTarif) ProtoMessage() {}

func (x *UpdateTarif) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateTarif.ProtoReflect.Descriptor instead.
func (*UpdateTarif) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{2}
}

func (x *UpdateTarif) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateTarif) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *UpdateTarif) GetModelId() string {
	if x != nil {
		return x.ModelId
	}
	return ""
}

func (x *UpdateTarif) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UpdateTarif) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *UpdateTarif) GetDayLimit() int64 {
	if x != nil {
		return x.DayLimit
	}
	return 0
}

func (x *UpdateTarif) GetOverLimit() int64 {
	if x != nil {
		return x.OverLimit
	}
	return 0
}

func (x *UpdateTarif) GetInsurancePrice() int64 {
	if x != nil {
		return x.InsurancePrice
	}
	return 0
}

func (x *UpdateTarif) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *UpdateTarif) GetStatus() bool {
	if x != nil {
		return x.Status
	}
	return false
}

type TarifPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *TarifPrimaryKey) Reset() {
	*x = TarifPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TarifPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TarifPrimaryKey) ProtoMessage() {}

func (x *TarifPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TarifPrimaryKey.ProtoReflect.Descriptor instead.
func (*TarifPrimaryKey) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{3}
}

func (x *TarifPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetListTarifRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListTarifRequest) Reset() {
	*x = GetListTarifRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListTarifRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListTarifRequest) ProtoMessage() {}

func (x *GetListTarifRequest) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListTarifRequest.ProtoReflect.Descriptor instead.
func (*GetListTarifRequest) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{4}
}

func (x *GetListTarifRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListTarifRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListTarifRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListTarifResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count  int64    `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Tarifs []*Tarif `protobuf:"bytes,2,rep,name=Tarifs,proto3" json:"Tarifs,omitempty"`
}

func (x *GetListTarifResponse) Reset() {
	*x = GetListTarifResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tarif_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListTarifResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListTarifResponse) ProtoMessage() {}

func (x *GetListTarifResponse) ProtoReflect() protoreflect.Message {
	mi := &file_tarif_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListTarifResponse.ProtoReflect.Descriptor instead.
func (*GetListTarifResponse) Descriptor() ([]byte, []int) {
	return file_tarif_proto_rawDescGZIP(), []int{5}
}

func (x *GetListTarifResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListTarifResponse) GetTarifs() []*Tarif {
	if x != nil {
		return x.Tarifs
	}
	return nil
}

var File_tarif_proto protoreflect.FileDescriptor

var file_tarif_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x74, 0x61, 0x72, 0x69, 0x66, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0xcf, 0x02, 0x0a,
	0x05, 0x54, 0x61, 0x72, 0x69, 0x66, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x19, 0x0a, 0x08,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x70,
	0x72, 0x69, 0x63, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63,
	0x65, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x61, 0x79, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x64, 0x61, 0x79, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x1d,
	0x0a, 0x0a, 0x6f, 0x76, 0x65, 0x72, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x09, 0x6f, 0x76, 0x65, 0x72, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x27, 0x0a,
	0x0f, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61, 0x6e, 0x63, 0x65, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0e, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61, 0x6e, 0x63,
	0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73,
	0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0b,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12,
	0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0c, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x87,
	0x02, 0x0a, 0x0b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x54, 0x61, 0x72, 0x69, 0x66, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x19, 0x0a, 0x08,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x70,
	0x72, 0x69, 0x63, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63,
	0x65, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x61, 0x79, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x64, 0x61, 0x79, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x1d,
	0x0a, 0x0a, 0x6f, 0x76, 0x65, 0x72, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x09, 0x6f, 0x76, 0x65, 0x72, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x27, 0x0a,
	0x0f, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61, 0x6e, 0x63, 0x65, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0e, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61, 0x6e, 0x63,
	0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73,
	0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x97, 0x02, 0x0a, 0x0b, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x54, 0x61, 0x72, 0x69, 0x66, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74,
	0x6f, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x19,
	0x0a, 0x08, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x72,
	0x69, 0x63, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x61, 0x79, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x64, 0x61, 0x79, 0x4c, 0x69, 0x6d, 0x69, 0x74,
	0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x76, 0x65, 0x72, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x07,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x6f, 0x76, 0x65, 0x72, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x12,
	0x27, 0x0a, 0x0f, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61, 0x6e, 0x63, 0x65, 0x5f, 0x70, 0x72, 0x69,
	0x63, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0e, 0x69, 0x6e, 0x73, 0x75, 0x72, 0x61,
	0x6e, 0x63, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63,
	0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64,
	0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x22, 0x21, 0x0a, 0x0f, 0x54, 0x61, 0x72, 0x69, 0x66, 0x50, 0x72, 0x69, 0x6d, 0x61,
	0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x5b, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74,
	0x54, 0x61, 0x72, 0x69, 0x66, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06,
	0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66,
	0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65,
	0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x22, 0x5a, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x54, 0x61, 0x72,
	0x69, 0x66, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f,
	0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74,
	0x12, 0x2c, 0x0a, 0x06, 0x54, 0x61, 0x72, 0x69, 0x66, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x14, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x54, 0x61, 0x72, 0x69, 0x66, 0x52, 0x06, 0x54, 0x61, 0x72, 0x69, 0x66, 0x73, 0x42, 0x18,
	0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_tarif_proto_rawDescOnce sync.Once
	file_tarif_proto_rawDescData = file_tarif_proto_rawDesc
)

func file_tarif_proto_rawDescGZIP() []byte {
	file_tarif_proto_rawDescOnce.Do(func() {
		file_tarif_proto_rawDescData = protoimpl.X.CompressGZIP(file_tarif_proto_rawDescData)
	})
	return file_tarif_proto_rawDescData
}

var file_tarif_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_tarif_proto_goTypes = []interface{}{
	(*Tarif)(nil),                // 0: order_service.Tarif
	(*CreateTarif)(nil),          // 1: order_service.CreateTarif
	(*UpdateTarif)(nil),          // 2: order_service.UpdateTarif
	(*TarifPrimaryKey)(nil),      // 3: order_service.TarifPrimaryKey
	(*GetListTarifRequest)(nil),  // 4: order_service.GetListTarifRequest
	(*GetListTarifResponse)(nil), // 5: order_service.GetListTarifResponse
}
var file_tarif_proto_depIdxs = []int32{
	0, // 0: order_service.GetListTarifResponse.Tarifs:type_name -> order_service.Tarif
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_tarif_proto_init() }
func file_tarif_proto_init() {
	if File_tarif_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_tarif_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Tarif); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_tarif_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateTarif); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_tarif_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateTarif); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_tarif_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TarifPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_tarif_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListTarifRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_tarif_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListTarifResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_tarif_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_tarif_proto_goTypes,
		DependencyIndexes: file_tarif_proto_depIdxs,
		MessageInfos:      file_tarif_proto_msgTypes,
	}.Build()
	File_tarif_proto = out.File
	file_tarif_proto_rawDesc = nil
	file_tarif_proto_goTypes = nil
	file_tarif_proto_depIdxs = nil
}
