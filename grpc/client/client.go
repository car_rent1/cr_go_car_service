package client

import (
	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	CarActivityService() car_service.CarActivityServiceClient
}

type grpcClients struct {
	carActivityService car_service.CarActivityServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connCarActivityService, err := grpc.Dial(
		cfg.CARACTIVITYServiceHost+cfg.CARACTIVITYPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	return &grpcClients{
		carActivityService: car_service.NewCarActivityServiceClient(connCarActivityService),
	}, nil
}

func (g *grpcClients) CarActivityService() car_service.CarActivityServiceClient {
	return g.carActivityService
}
