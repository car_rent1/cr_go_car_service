package grpc

import (
	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/service"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	car_service.RegisterBrandServiceServer(grpcServer, service.NewBrandService(cfg, log, strg, srvc))

	car_service.RegisterModelServiceServer(grpcServer, service.NewModelService(cfg, log, strg, srvc))

	car_service.RegisterCarServiceServer(grpcServer, service.NewCarService(cfg, log, strg, srvc))

	car_service.RegisterCarActivityServiceServer(grpcServer, service.NewCarActivityService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
