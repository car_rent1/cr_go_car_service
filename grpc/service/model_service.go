package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type ModelService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*car_service.UnimplementedModelServiceServer
}

func NewModelService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ModelService {
	return &ModelService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ModelService) Create(ctx context.Context, req *car_service.CreateModel) (resp *car_service.Model, err error) {

	u.log.Info("-----------CreateModel----------", logger.Any("req", req))

	id, err := u.strg.Model().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateModel ->Model->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Model().GetByID(ctx, id)
	if err != nil {
		u.log.Error("GetByIdModel->GetById", logger.Error(err))
		return
	}

	return
}

func (u *ModelService) GetByID(ctx context.Context, req *car_service.ModelPrimaryKey) (resp *car_service.Model, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Model().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Model->GetByID", logger.Error(err))
		return
	}

	return 
}

func (u *ModelService) GetList(ctx context.Context, req *car_service.GetListModelRequest) (*car_service.GetListModelResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Model().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Model->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ModelService) Update(ctx context.Context, req *car_service.UpdateModel) (resp *car_service.Model, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Model().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Model->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Model().GetByID(ctx, &car_service.ModelPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Model->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *ModelService) Delete(ctx context.Context, req *car_service.ModelPrimaryKey) (resp *car_service.ModelEmpty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Model().Delete(ctx, &car_service.ModelPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Model->Delete", logger.Error(err))
		return
	}

	return
}
