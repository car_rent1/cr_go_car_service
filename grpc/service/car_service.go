package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedCarServiceServer
	*car_service.UnimplementedCarActivityServiceServer
}

func NewCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CarService {
	return &CarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CarService) Create(ctx context.Context, req *car_service.CreateCar) (resp *car_service.Car, err error) {
	i.log.Info("---CreateCar------>", logger.Any("req", req))

	pKey, err := i.strg.Car().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCar->Car->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Car().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCar->Car->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.strg.CarActivity().Create(ctx, &car_service.CarActivityCreate{
		StateNumber: resp.StateNumber,
		CarId:       resp.Id,
		Status:      resp.Status,
		Date:        resp.CreatedAt,
	})
	if err != nil {
		i.log.Error("!!!CarActivityCreate->Car->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *CarService) GetByID(ctx context.Context, req *car_service.CarPrimaryKey) (resp *car_service.Car, err error) {

	i.log.Info("---GetCarByID------>", logger.Any("req", req))

	resp, err = i.strg.Car().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCarByID->Car->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CarService) GetList(ctx context.Context, req *car_service.GetListCarRequest) (*car_service.GetListCarResponse, error) {
	i.log.Info("-------GetListCar-------", logger.Any("req", req))

	resp, err := i.strg.Car().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->Car->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *CarService) Update(ctx context.Context, req *car_service.UpdateCar) (resp *car_service.Car, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Car().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Car().GetByPKey(ctx, &car_service.CarPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateCar->Car->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CarService) Delete(ctx context.Context, req *car_service.CarPrimaryKey) (resp *car_service.CarEmpty, err error) {

	i.log.Info("---DeleteCar------>", logger.Any("req", req))

	resp, err = i.strg.Car().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCar->Car->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
