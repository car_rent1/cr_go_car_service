package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GiveCarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedGiveCarServiceServer
}

func NewGiveCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *GiveCarService {
	return &GiveCarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *GiveCarService) Create(ctx context.Context, req *car_service.CreateGiveCar) (resp *car_service.GiveCar, err error) {
	i.log.Info("---CreateGiveCar------>", logger.Any("req", req))

	pKey, err := i.strg.GiveCar().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateGiveCar->GiveCar->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.GiveCar().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *GiveCarService) GetByID(ctx context.Context, req *car_service.GiveCarPrimaryKey) (resp *car_service.GiveCar, err error) {

	i.log.Info("---GetGiveCarByID------>", logger.Any("req", req))

	resp, err = i.strg.GiveCar().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetGiveCarByID->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *GiveCarService) GetList(ctx context.Context, req *car_service.GetListGiveCarRequest) (*car_service.GetListGiveCarResponse, error) {
	i.log.Info("-------GetListGiveCar-------", logger.Any("req", req))

	resp, err := i.strg.GiveCar().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->GiveCar->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *GiveCarService) Update(ctx context.Context, req *car_service.UpdateGiveCar) (resp *car_service.GiveCar, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.GiveCar().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.GiveCar().GetByPKey(ctx, &car_service.GiveCarPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *GiveCarService) Delete(ctx context.Context, req *car_service.GiveCarPrimaryKey) (resp *car_service.GiveCarEmpy, err error) {

	i.log.Info("---DeleteGiveCar------>", logger.Any("req", req))

	resp, err = i.strg.GiveCar().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
