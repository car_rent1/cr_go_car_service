package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
)

type BrandService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*car_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BrandService {
	return &BrandService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *BrandService) Create(ctx context.Context, req *car_service.CreateBrand) (resp *car_service.Brand, err error) {

	u.log.Info("-----------CreateBrand----------", logger.Any("req", req))

	id, err := u.strg.Brand().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateBrand ->Brand->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Brand().GetByID(ctx, id)
	if err != nil {
		u.log.Error("GetByIdBrand->GetById", logger.Error(err))
		return
	}

	return
}

func (u *BrandService) GetById(ctx context.Context, req *car_service.BrandPrimaryKey) (resp *car_service.Brand, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Brand().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Brand->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *BrandService) GetList(ctx context.Context, req *car_service.GetListBrandRequest) (*car_service.GetListBrandResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Brand().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Brand->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *BrandService) Update(ctx context.Context, req *car_service.UpdateBrand) (resp *car_service.Brand, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Brand().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Brand->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Brand().GetByID(ctx, &car_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Brand->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *BrandService) Delete(ctx context.Context, req *car_service.BrandPrimaryKey) (resp *car_service.BrandEmpty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Brand().Delete(ctx, &car_service.BrandPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Brand->Delete", logger.Error(err))
		return
	}

	return
}
