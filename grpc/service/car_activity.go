package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_car_service/config"
	"gitlab.com/car_rent1/cr_go_car_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_car_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_car_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_car_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CarActivityService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedCarActivityServiceServer
}

func NewCarActivityService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CarActivityService {
	return &CarActivityService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}
func (i *CarActivityService) Create(ctx context.Context, req *car_service.CarActivityCreate) (*car_service.CarActivity, error) {
	i.log.Info("-------CreateCarActivity-------", logger.Any("req", req))
	resp, err := i.strg.CarActivity().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!Create->CarActivity->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil

}
func (i *CarActivityService) GetList(ctx context.Context, req *car_service.CarActivityGetListRequest) (*car_service.CarActivityResponse, error) {
	i.log.Info("-------GetListCarActivity-------", logger.Any("req", req))

	resp, err := i.strg.CarActivity().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->CarActivity->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	carResp := []*car_service.CarActivityResp{}
	hash := map[string][]*car_service.CarActivity{}

	for _, temp := range resp.Res {
		_, ok := hash[temp.StateNumber]
		if ok {
			hash[temp.StateNumber] = append(hash[temp.StateNumber], temp)
		} else {
			hash[temp.StateNumber] = []*car_service.CarActivity{temp}
		}
	}

	for i, val := range hash {
		carResp = append(carResp, &car_service.CarActivityResp{
			StateNumber: i,
			Res:         val,
		})
	}

	response := &car_service.CarActivityResponse{
		CarResp: carResp,
	}

	return response, nil
}
